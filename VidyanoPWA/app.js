"use strict";
var VidyanoPWA;
(function (VidyanoPWA) {
    var AppServiceHooks = /** @class */ (function (_super) {
        __extends(AppServiceHooks, _super);
        function AppServiceHooks() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        AppServiceHooks.prototype.onConstructQuery = function (service, query, parent, asLookup, maxSelectedItems) {
            var toReturn = _super.prototype.onConstructQuery.call(this, service, query, parent, asLookup, maxSelectedItems);
            if (query.name === "Customers") {
                this.GetItems(query);
            }
            return toReturn;
        };
        AppServiceHooks.prototype.GetItems = function (query) {
            return __awaiter(this, void 0, void 0, function () {
                var user, token, ajax;
                return __generator(this, function (_a) {
                    user = !!localStorage.getItem("/userName") ? JSON.parse(localStorage.getItem("/userName")).val : JSON.parse(sessionStorage.getItem("/userName")).val;
                    token = !!localStorage.getItem("/authToken") ? JSON.parse(localStorage.getItem("/authToken")).val : JSON.parse(sessionStorage.getItem("/authToken")).val;
                    ajax = new AJAX("POST", this.app.baseURI + "api/GetAllPersistentObjectForQuery", JSON.stringify({
                        queryName: "Customers",
                        userName: user,
                        authToken: token
                    }), this.app, query.persistentObject.id);
                    ajax._send();
                    return [2 /*return*/];
                });
            });
        };
        return AppServiceHooks;
    }(Vidyano.WebComponents.AppServiceHooks));
    VidyanoPWA.AppServiceHooks = AppServiceHooks;
    var AJAX = /** @class */ (function () {
        function AJAX(method, url, params, app, queryPoId) {
            this.method = method;
            this.url = url;
            this.params = params;
            this.app = app;
            this.queryPoId = queryPoId;
        }
        AJAX.prototype._init = function () {
            if (typeof XMLHttpRequest != 'undefined') {
                this.server = new XMLHttpRequest();
                this.server.open(this.method, this.url, true);
                return true;
            }
        };
        AJAX.prototype._send = function () {
            var _this = this;
            if (this._init()) {
                this.server.open(this.method, this.url, true);
                this.server.onload = function () {
                    if (_this.server.readyState === _this.server.DONE && _this.server.status === 200) {
                        var vidyanoParams = {
                            clientVersion: Vidyano.version,
                            environment: _this.app.service.environment,
                            environmentVersion: _this.app.service.environmentVersion,
                            persistentObjectTypeId: _this.queryPoId,
                            serverName: _this.app.baseURI,
                            appPath: _this.app.path
                        };
                        localStorage.setItem("vidyanoParams", JSON.stringify(vidyanoParams));
                        localStorage.setItem("poItems", _this.server.responseText);
                    }
                    else if (_this.server.status !== 200) {
                        alert('Request failed.  Returned status of ' + _this.server.status);
                    }
                };
                this.server.send(this.params);
            }
        };
        return AJAX;
    }());
})(VidyanoPWA || (VidyanoPWA = {}));
