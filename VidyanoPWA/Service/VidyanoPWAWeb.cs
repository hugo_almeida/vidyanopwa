using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using Vidyano.Service;
using Vidyano.Service.Repository;

namespace VidyanoPWA.Service
{
    [RoutePrefix("api")]
    public class VidyanoPWAWeb : CustomApiController
    {
        [HttpGet]
        [Route("GetAllPersistentObjectForQuery")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage GetAllPersistentObjectForQuery(ApiArgs args)
        {
            var obj = JsonConvert.DeserializeObject<RequestPayload>(args.Request.Content.ReadAsStringAsync().Result);

            if(Manager.Current.User == null)
                Manager.CreateForUser(obj.UserName);

            // Execute query with all items
            var query = Manager.Current.GetQuery(obj.QueryName);
            var queryExecuted = Manager.Current.ExecuteQuery(query);

            // get the total items from query
            //query.PageSize = queryExecuted.TotalItems;
            //queryExecuted = Manager.Current.ExecuteQuery(query);


            var jsonPos = new List<PersistentObject>();
            foreach(var item in queryExecuted.Items)
            {   
                var po = Manager.Current.GetPersistentObject(query.PersistentObject.FullTypeName, item.Id, true);
                jsonPos.Add(po);
            }

            var jsonPosArr = jsonPos.ToArray();
            var aux = JsonConvert.SerializeObject(jsonPosArr);

            return new HttpResponseMessage
            {
                Content = new StringContent(aux, Encoding.UTF8, "application/json")
            };
        }

        private class RequestPayload
        {
            public string QueryName { get; set; }
            public string UserName { get; set; }
            public string AuthToken { get; set; }
        }
    }
}