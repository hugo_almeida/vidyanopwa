﻿module VidyanoPWA {
    export class AppServiceHooks extends Vidyano.WebComponents.AppServiceHooks {
        private server: XMLHttpRequest;

        onConstructQuery(service: Object, query, parent?: Vidyano.PersistentObject, asLookup?: boolean, maxSelectedItems?: number): Vidyano.Query {
            const toReturn = super.onConstructQuery(service as any, query, parent, asLookup, maxSelectedItems);
            if (query.name === "Customers") {
                this.GetItems(query);
            }
            return toReturn;
        }

        private async GetItems(query: any) {
            const user = !!localStorage.getItem("/userName") ? JSON.parse(localStorage.getItem("/userName")).val : JSON.parse(sessionStorage.getItem("/userName")).val;
            const token = !!localStorage.getItem("/authToken") ? JSON.parse(localStorage.getItem("/authToken")).val : JSON.parse(sessionStorage.getItem("/authToken")).val;

            const ajax = new AJAX(
                "POST",
                `${this.app.baseURI}api/GetAllPersistentObjectForQuery`,
                JSON.stringify({
                    queryName: "Customers",
                    userName: user,
                    authToken: token
                }),
                this.app,
                query.persistentObject.id);
            ajax._send();
        }

    }

    class AJAX {
        private server: XMLHttpRequest;
        private method: string;
        private url: string;
        private params: string;
        private app: Vidyano.WebComponents.App;
        private queryPoId: string;

        constructor(method: string, url: string, params: string, app: Vidyano.WebComponents.App, queryPoId: string) {
            this.method = method;
            this.url = url;
            this.params = params;
            this.app = app;
            this.queryPoId = queryPoId;
        }

        _init() {
            if (typeof XMLHttpRequest != 'undefined') {
                this.server = new XMLHttpRequest();
                this.server.open(this.method, this.url, true);
                return true;
            }
        }

        _send() {
            if (this._init()) {
                this.server.open(this.method, this.url, true);
                this.server.onload = () => {
                    if (this.server.readyState === this.server.DONE && this.server.status === 200) {
                        const vidyanoParams = {
                            clientVersion: Vidyano.version,
                            environment: this.app.service.environment,
                            environmentVersion: this.app.service.environmentVersion,
                            persistentObjectTypeId: this.queryPoId,
                            serverName: this.app.baseURI,
                            appPath: this.app.path
                        };
                        localStorage.setItem("vidyanoParams", JSON.stringify(vidyanoParams));
                        localStorage.setItem("poItems", this.server.responseText);
                    }
                    else if (this.server.status !== 200) {
                        alert('Request failed.  Returned status of ' + this.server.status);
                    }

                }; 
                this.server.send(this.params);
            }
        }
    }

}

