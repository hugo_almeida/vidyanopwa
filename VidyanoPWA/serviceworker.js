﻿var cacheName = "vidyano-pwa-cache";
var cachePostsResponses = [];

// listener to fetch event of service worker - catch every request to network
self.addEventListener('fetch', function (event) {
    // answer with
    event.respondWith(
        // open/create cache
        caches.open(cacheName).then(function (cache) {
            // checks if the request already exists in the cache
            return cache.match(event.request).then(async function (response) {
                if (event.request.method === "POST") {
                    if (event.request.url.includes("GetPersistentObject")) {
                        var data = JSON.parse(localStorage.getItem("poItems"));
                        data.forEach(d => {
                            var req = requestResponsePair.getRequest();
                        });

                    }

                    const cachedResponse = cachePostsResponses.find(r => event.request.url === r.name);
                    return !!cachedResponse ? cachedResponse.response.clone() :
                        fetch(event.request).then(function (response) {
                            cachePostsResponses.push({ name: response.url, response: response.clone() });
                            return response;
                        });
                }

                return response || fetch(event.request).then(function (response) {
                    cache.put(event.request, response.clone());
                    return response;
                });


            });
        })
    );
});

var requestResponsePair = function () {

    var vidyanoParams = JSON.parse(localStorage.getItem("vidyanoParams"));

    getRequest = () => {
        return new Request(`${serverName}GetPersistentObject`,
            {
                method: "POST",
                bodyUsed: false,
                body: {
                    authToken: JSON.parse(localStorage.getItem("/authToken")).val,
                    clientVersion: vidyanoParams.clientVersion,
                    environment: vidyanoParams.environment,
                    environmentVersion: vidyanoParams.environmentVersion,
                    objectId: item.id,
                    persistentObjectTypeId: vidyanoParams.persistentObjectTypeId,
                    userName: JSON.parse(localStorage.getItem("/userName")).val
                },
                cache: "default",
                credentials: "same-origin",
                destination: "",
                headers: getHeaders(),
                integrity: "",
                morde: "cors",
                redirect: "follow",
                referrer: `${vidyanoParams.serverName}Home/Customers`,
                referrerPolicy: "no-referrer-when-downgrade",
                url: `${vidyanoParams.serverName}GetPersistentObject`
            });
    };

    getResponse = (item) => {
        return  new Response(
            {
                result: item
            },
            {
                status: 200,
                statusText: "OK"
            });
    };

    getHeaders = () => {
        const myHeaders = new Headers();
        myHeaders.append("Content-Type", "text/plain;charset=UTF-8");
        myHeaders.append("Origin", vidyanoParams.serverName);
        myHeaders.append("Referer", vidyanoParams.serverName + vidyanoParams.appPath);
        myHeaders.append("User-Agent", navigator.userAgent);
        return myHeaders;
    };

};